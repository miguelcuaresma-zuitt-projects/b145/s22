//JSON Objects
// JSON stands for JavaScript Object Notation
// JSON is a data format used by JavaScript as well as other programming languages
// JSON Objects are NOT to be confused with JavaScript Objects
// use of double quotation marks is required in JSON Objects
/*
Syntax:

{
	"propertyA": "valueA",
	"propertyB": "valueB"
}

*/

/*
{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/

/*
	"number": 1,
	"boolean" true
*/

//JavaScript Array of Objects:
let myArr = [
	{name: "Jino"},
	{name: "John"}
]

//Array of JSON Objects
/*
{
	"cities": [
		{"city": "Quezon City"},
		{"city": "Makati City"}
	]
}
*/

// Converting JS Data into stringified JSON
// Stringified JSON is a JavaScript Object converted into a string to be used by the receiving back-end application or other function of a JavaScript application.

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'}
];

let stringifiedData = JSON.stringify(batchesArr);


let fixedData = JSON.parse(stringifiedData);